YT_ID=9ijFgJnTh1M
BASENAME=_$(YT_ID)

default: all
all: pages sep.rnc

pages:
	mkdir -p html
	cd html && $(MAKE)

sep.rnc: $(BASENAME).sep
	trang -I xml $< $@

$(BASENAME).sbv: $(BASENAME).sep sep2sbv.js
	@echo "Attention, ceci peut écraser votre travail récent !"
	@if [[ $@ -nt $< ]] ; then \
		echo "$@ est plus récent que $<"; \
	else \
		echo "$@ est plus ancien que $<"; \
	fi
	@echo "  Pour régénerer $@ à partir de $<, utilisez la commande suivante: "
	@echo node  sep2sbv.js

$(BASENAME).en.sbv: $(BASENAME).sep gen-translation-sbv.js
	node gen-translation-sbv.js


# %.lines: %.sbv
# 	sed -e '/^0:/ d' $< > $@

%.txt:%.sbv
	cat $< | node sbv2-wrapped-text.js > $@
