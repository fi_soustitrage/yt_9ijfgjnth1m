# Travaux de sous-titrage FI

## Besoins

  - choisir librement les vidéos à travailler
  - communication avec les contributeurs avec les techniques de gestion de
    projet (ex: github issues)
  - suivi de versions
     - pour ne rien perdre (pouvoir revenir à une version antérieure, combiner
       des versions différentes)
  - wiki et pages github pour faciliter les échanges sur les techniques de
    travail, mettre au point et consolider des choix de style
  - faciliter le travail en commun


## Video YT#9ijFgJnTh1M - "Affaire Benalla - Nous Ne Nous Tairons Pas"

![Vue Traducteur](doc/images/TranslationView.png)


  - Voir dans Youtube: https://www.youtube.com/watch?v=9ijFgJnTh1M
  - Editeur de sous-titres de YT:
    - [transcription (fr)](https://www.youtube.com/timedtext_editor?action_mde_edit_form=1&v=9ijFgJnTh1M&lang=fr&bl=csps&ui=sub&ref=hub&tab=captions)
      - [english version](https://www.youtube.com/timedtext_editor?action_mde_edit_form=1&v=9ijFgJnTh1M&lang=en&bl=csps&ui=sub&ref=hub&tab=captions)

### Démarche
  Cette vidéo est en tendance. Mon ordi étant configuré principalement en anglais,
J'ai vu le sous-titrage automatique en anglais, un trésor de non-sens !

C'est pourquoi j'ai commencé à corriger ces sous-titres, directement dans une
version `.srt`. C'est long !

Pour soumettre mon travail, le plus efficace actuellement m'a semblé être
directement l'outil YouTube "timedtext_editor", mais pour le travail collectif,
il y a peu de facilités.

Les outils de traduction et de traduction automatiques peuvent cependant être
une bonne base de départ, à condition d'être bien utilisés.

La version initiale de la transcription française était relativement acceptable,
mais j'ai réalisé que soigner d'abord cette transcription de base, dans sa
grammaire, sa sémantique et son phrasé devrait faciliter grandement les
traductions. C'est ce qui a orienté mes choix stylistiques, au risque de
sembler parfois alambiqué pour certains. Mais il peut très certainement être
amélioré (pour les majuscules, je me suis référée à
[ça](http://j.poitou.free.fr/pro/html/typ/cap-emplois.html),
mais il y aussi [ça](https://www.lalanguefrancaise.com/general/guide-complet-usage-majuscules-francais/))

Le suivi de versions me semble essentiel, j'ai donc créé un dossier `.git` sur
mon ordi. Je travaille sous Linux, principalement entre Emacs et le Terminal.

Pour l'ajustement temporel des sous-titres, j'ai choisi
[SubtitleEditor](https://kitone.github.io/subtitleeditor/) .


### Travail sur le texte.

Je vais tenter de maintenir une [version HTML](html/_9ijFgJnTh1M.html) des transcriptions, qui permette de
retenir les infos de timing sans gêner la lecture (à usage des correcteurs
principalement).

Dans les phases initiales, j'ai trouvé plus pratique de travailler sur le `.sbv`
dans mon éditeur de texte, sans toucher aux infos de timing, mais en
fractionnant les longues lignes, en pensant sémantique et phrasé (fait en écoutant la
vidéo, pour me baser sur l'intonation).

A cette étape, j'ai choisi plutôt un fractionnement élevé, mais sans jamais
déplacer du texte d'un bloc à l'autre.

La recombinaison sera facile sous SubtitleEditor.

### Travail sur le timing.
Hélas, SubtitleEditor n'est pas prolixe en documentation, donc on apprend en
faisant.

Choisir le mode d'affichage "Timing" et repérer les actions les plus utiles:
  - Split :: fractionne un bloc en autant de morceaux que de lignes
  - Combine :: l'inverse: sélectionner plusieurs blocs pour obtenir un bloc
    multilignes.

Configurer les raccourcis clavier correspondants, en priorité, pour aller plus
vite.

Repérer aussi les avertissements, marques en rouge sur les blocs demandant des
ajustements.

Optionnel: générer une "waveform" pour visualiser les blocs à la manière de
*timedtext_editor* de YT. (ce peut être un peu long, donc on a intérêt à
sauvegarder le .wf, pour réutilisation).

Les premières actions utilisées
Plus tard, ces fragments de lignes pourront être facilement sépar

### A propos de moi
Avertissement : jusqu'ici, j'utilisais principalement github. Je pense que
bitbucket est mieux pour ce travail. Il permet de grouper les dépôts, et au
besoin de contrôler finement les conditions d'accès. Mais il me faudra un peu de
temps pour apprendre à l'utiliser au mieux. Vos suggestions sont bienvenues !

Je demande aussi votre indulgence pour la langue. Je suis bien française de
langue maternelle, mais souvent en informatique, je travaille, et pense, en
anglais. Mon ordi est configuré en anglais aussi, donc il peux m'arriver de
faire référence à des éléments en anglais ou me tromper en tentant une
traduction à la volée.
Je suis plus technicienne que littéraire, mais je fais de mon mieux ...

### Travail collaboratif
Dans un premier temps, je propose d'utiliser surtout le
["Gestionnaire de Bugs"](https://bitbucket.org/Talulla/soustitresfi/issues?status=new&status=open)
