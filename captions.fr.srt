1
00:00:00,000 --> 00:00:00,335
Bonjour

2
00:00:00,435 --> 00:00:03,218
Je m'adresse à vous parce que nous vivons un moment 

3
00:00:03,318 --> 00:00:06,080
singulier et grave de la vie politique de notre pays

4
00:00:06,180 --> 00:00:09,132
 avec ce qu'on a appelé "l'affaire Benalla-Macron". 

5
00:00:09,233 --> 00:00:12,459
Vous savez comme moi qu'il y a eu une vidéo qui a montré

6
00:00:12,740 --> 00:00:16,320
une personne qui n'est pas un policier se jeter sur des gens,

7
00:00:17,500 --> 00:00:21,119
les frapper, et tout ça en dehors de tout cadre légal.

8
00:00:21,660 --> 00:00:24,180
Je ne dis pas que ce soit bien de frapper des gens dans le cadre légal,

9
00:00:24,460 --> 00:00:27,561
mais c'est encore plus détestable quand ce sont des gens 

10
00:00:27,661 --> 00:00:30,489
qui n'ont aucune autorité de police, 

11
00:00:30,589 --> 00:00:33,935
qui procèdent à des interpellations de cette nature.

12
00:00:34,035 --> 00:00:36,238
Bon, tout ça, vous le connaissez.

13
00:00:36,940 --> 00:00:39,930
On a eu à la suite de la diffusion de cette vidéo

14
00:00:41,980 --> 00:00:43,980
plusieurs jours étranges.

15
00:00:44,320 --> 00:00:47,070
Aucun responsable ne voulait répondre, personne !

16
00:00:48,219 --> 00:00:51,841
Le premier ministre a préféré
commenter une étape du Tour de France plutôt 

17
00:00:51,941 --> 00:00:53,900
que de répondre à l'appel de l'Assemblée Nationale, 

18
00:00:54,000 --> 00:00:57,670
qui a le droit constitutionnel
de lui demander de venir,

19
00:00:57,670 --> 00:00:59,260
n'a pas répondu.

20
00:00:59,260 --> 00:01:02,760
Nous lui avons mis cette demande par écrit, il a répondu:

21
00:01:03,040 --> 00:01:05,260
"Ce n'est pas opportun, je ne viens pas".

22
00:01:05,260 --> 00:01:07,260
Comme il n'avait pas moyen d'y échapper,

23
00:01:07,390 --> 00:01:11,879
il a été obligé de passer à la série des questions dites d'actualité.

24
00:01:12,549 --> 00:01:14,938
Alors je sais que du point de vue du spectacle,

25
00:01:16,180 --> 00:01:18,834
c'est une bonne chose, c'est plaisant à regarder; 

26
00:01:18,934 --> 00:01:21,646
on voit plusieurs questions posées et des réponses, 

27
00:01:21,746 --> 00:01:23,697
mais je voudrais que vous sachiez que

28
00:01:24,790 --> 00:01:26,790
c'est totalement insuffisant.

29
00:01:27,549 --> 00:01:29,431
Chacun d'entre nous a deux minutes

30
00:01:29,531 --> 00:01:32,890
 pour interpeller le premier ministre, deux minutes !

31
00:01:32,890 --> 00:01:34,543
La France Insoumise, qui représente 

32
00:01:34,643 --> 00:01:36,120
plusieurs millions de personnes, 

33
00:01:36,220 --> 00:01:39,430
a eu deux minutes de temps de parole pour

34
00:01:39,430 --> 00:01:40,763
interpeller le premier ministre, 

35
00:01:40,863 --> 00:01:42,658
qui lui a le temps qu'il veut pour répondre.

36
00:01:43,149 --> 00:01:45,089
Je vous le précise pour dire que ceci 

37
00:01:45,189 --> 00:01:47,968
n'est pas une manière normale de vie des institutions,

38
00:01:48,729 --> 00:01:49,972
de même qu'il n'est pas normal 

39
00:01:50,072 --> 00:01:52,308
que le ministre chargé des relations avec le parlement, 

40
00:01:52,408 --> 00:01:53,067
c'est son boulot,

41
00:01:53,619 --> 00:01:56,155
ne vienne pas dans l'hémicycle quand toute l'Assemblée dit 

42
00:01:56,255 --> 00:01:57,958
"mais nous voulons entendre quelqu'un !"

43
00:01:59,079 --> 00:02:02,279
Et ce ministre, où il était ? Dans les couloirs, en train de

44
00:02:03,430 --> 00:02:06,524
passer à des altercations avec des députés qui se trouvaient là, 

45
00:02:06,624 --> 00:02:07,348
ou à la buvette,

46
00:02:07,790 --> 00:02:09,858
mais nous ne l'avons pas vu dans l'hémicycle, 

47
00:02:09,958 --> 00:02:13,725
à aucun moment il n'a porté la parole du gouvernement
devant nous. 

48
00:02:13,825 --> 00:02:15,827
Ça déjà, c'est pas des formes acceptables :

49
00:02:15,927 --> 00:02:17,539
 dans tous les parlements du monde

50
00:02:18,180 --> 00:02:19,550
avec une affaire pareille

51
00:02:19,550 --> 00:02:21,030
les ministres concernés viennent. 

52
00:02:21,130 --> 00:02:22,790
Là c'était le ministre de l'intérieur,

53
00:02:22,860 --> 00:02:24,364
au bout de deux jours, on voit bien que 

54
00:02:24,464 --> 00:02:26,239
ça ne pouvait plus être le Ministre l'Intérieur

55
00:02:26,480 --> 00:02:27,453
mais le Premier Ministre,

56
00:02:27,553 --> 00:02:29,748
 ou le ministre chargé des relations avec le Parlement. 

57
00:02:29,848 --> 00:02:29,998
Rien.

58
00:02:30,560 --> 00:02:34,038
J'y insiste, vous ne pouvez pas accepter 
que les choses se passent comme ça,

59
00:02:34,138 --> 00:02:35,619
parce que ça, c'est la monarchie:

60
00:02:36,420 --> 00:02:37,581
le monarque se tait

61
00:02:37,681 --> 00:02:39,748
 et demande à tous ses subordonnés

62
00:02:39,848 --> 00:02:40,959
 d'en faire autant,

63
00:02:41,059 --> 00:02:42,858
 et donc pendant trois jours,

64
00:02:43,260 --> 00:02:46,129
on ne sait rien et on est condamnés, nous, parlementaires, 

65
00:02:46,229 --> 00:02:48,850
à intervenir en rafale pour demander des explications, 

66
00:02:48,950 --> 00:02:51,504
ce qui permet ensuite
à la majorité de dire : 

67
00:02:51,604 --> 00:02:53,826
"regardez, voyez leur comportement !". 

68
00:02:53,926 --> 00:02:55,710
Mais ce comportement a été provoqué 

69
00:02:55,810 --> 00:02:56,809
de manière délibérée 

70
00:02:56,909 --> 00:02:57,699
par la majorité. 

71
00:02:57,799 --> 00:02:58,848
Au bout de dix heures

72
00:02:59,160 --> 00:03:02,268
nous avons obtenu la création 

73
00:03:02,368 --> 00:03:05,110
d'une commission d'enquête 

74
00:03:05,210 --> 00:03:06,699
à l'Assemblée Nationale. 

75
00:03:06,799 --> 00:03:07,829
Juste avant, la même chose avait été décidée au Sénat.

76
00:03:07,830 --> 00:03:10,110
Dix heures de pression pour obtenir ça ! 

77
00:03:10,210 --> 00:03:13,968
Une commission d'enquête, ce n'est pas une instance administrative,

78
00:03:14,370 --> 00:03:16,044
ce n'est pas non plus un tribunal, 

79
00:03:16,144 --> 00:03:17,670
et tous ceux qui viennent devant 

80
00:03:17,770 --> 00:03:18,852
ne courent aucun risque 

81
00:03:18,952 --> 00:03:19,936
s'ils ne mentent pas. 

82
00:03:20,036 --> 00:03:21,168
S'ils mentent évidemment

83
00:03:21,600 --> 00:03:23,245
ils courent le risque du mensonge. 

84
00:03:23,345 --> 00:03:25,668
C'est d'ailleurs pourquoi on a demandé à entendre

85
00:03:26,220 --> 00:03:29,600
le Président de la République, parce que plusieurs constitutionnalistes

86
00:03:30,150 --> 00:03:31,925
sont d'accord avec nous pour dire que, 

87
00:03:32,025 --> 00:03:33,984
dans la mesure où une commission d'enquête, 

88
00:03:34,084 --> 00:03:35,719
qui est, là, la Commission des Lois,

89
00:03:36,180 --> 00:03:38,603
ne porte pas de jugement, ne vote pas,

90
00:03:38,703 --> 00:03:40,100
 n'a pas d'appréciation

91
00:03:40,200 --> 00:03:44,505
 sur les témoignages et les auditions
qu'elle recueille, 

92
00:03:44,605 --> 00:03:46,809
et bien le Président de la République peut venir. 

93
00:03:46,909 --> 00:03:49,578
Après tout c'est quand même lui qui a dit qu'il était prêt,

94
00:03:50,250 --> 00:03:52,658
devant le congrès à Versailles, 
c'est lui qui a dit 

95
00:03:52,758 --> 00:03:56,207
"Dorénavant je resterai, j'écouterai ce qu'on me dit et puis je répondrai"

96
00:03:56,489 --> 00:03:58,184
donc il est d'accord pour répondre, 

97
00:03:58,284 --> 00:04:00,025
sans vote, 
devant des parlementaires. 

98
00:04:00,125 --> 00:04:01,577
Pourquoi ne le fait-il pas là ?

99
00:04:02,610 --> 00:04:05,038
Il n'a rien dit, à aucune occasion,
et hier soir, 

100
00:04:05,138 --> 00:04:07,623
à l'occasion d'un pot de fin d'année 

101
00:04:07,723 --> 00:04:10,398
des députés de La République En Marche,

102
00:04:10,830 --> 00:04:12,830
il a surgi et fait un discours.

103
00:04:13,910 --> 00:04:16,749
On peut beaucoup se poser des questions sur cette façon de procéder.

104
00:04:17,959 --> 00:04:20,315
Mais on s'en pose d'autant plus

105
00:04:20,415 --> 00:04:22,538
qu' il y avait autour de lui

106
00:04:23,120 --> 00:04:25,405
quelques-uns des principaux personnages de l'État: 

107
00:04:25,505 --> 00:04:27,173
le président de l'Assemblée Nationale, 

108
00:04:27,273 --> 00:04:30,308
qui applaudissait, certes, fort mollement, 
 mais qui applaudissait;

109
00:04:30,500 --> 00:04:32,380
la ministre de la Justice,

110
00:04:32,480 --> 00:04:36,065
alors que vont être évoquées
devant elle 

111
00:04:36,165 --> 00:04:40,072
des questions judiciaires qui relèvent de de son autorité. 

112
00:04:40,172 --> 00:04:41,888
Alors, c'est la même femme

113
00:04:42,980 --> 00:04:45,935
qui était présente à l'Assemblée Nationale et qui disait: 

114
00:04:46,035 --> 00:04:48,368
"moi je ne peux m'exprimer sur rien, parce que

115
00:04:49,040 --> 00:04:51,767
il y a des instructions judiciaires qui sont en cours". 

116
00:04:51,867 --> 00:04:55,238
Et combien d'autres qui sont là, sur les images si vous les regardez,

117
00:04:55,790 --> 00:04:59,049
autour du Président de la République qui prend la parole et fait un discours.

118
00:04:59,720 --> 00:05:02,071
C'est donc un discours de pot de fin d'année 

119
00:05:02,171 --> 00:05:05,109
qui nous permet de savoir ce que le Chef de l'État pense.

120
00:05:06,500 --> 00:05:08,004
Ça aussi, c'est pas banal. 

121
00:05:08,104 --> 00:05:10,421
Ça manifeste même en un mépris incroyable,

122
00:05:10,521 --> 00:05:13,867
 non seulement pour le reste de la représentation nationale

123
00:05:14,600 --> 00:05:16,600
mais aussi pour les citoyens

124
00:05:17,120 --> 00:05:19,299
comme on est dans la confusion la plus complète

125
00:05:20,240 --> 00:05:26,470
à ce moment là on a pu voir des membres de la commission qui enquête sur les événements

126
00:05:27,050 --> 00:05:29,050
être devant le chef de l'état

127
00:05:29,360 --> 00:05:31,870
qui refuse de venir en audition devant la commission

128
00:05:33,530 --> 00:05:35,530
Entendre des mises en cause

129
00:05:36,440 --> 00:05:38,739
Dont sept du parlement dont ils sont membres

130
00:05:39,440 --> 00:05:45,640
et entendre des compliments faits à monsieur alexandre benalla jusqu'au point que

131
00:05:46,100 --> 00:05:49,059
le public qui se trouve là se met à l'applaudir

132
00:05:49,820 --> 00:05:51,350
et tout ça

133
00:05:51,350 --> 00:05:57,670
sans que personne dans cette salle de grands défenseurs de la morale de la séparation des pouvoirs

134
00:05:58,210 --> 00:06:03,970
eux qui nous ont donné toutes les leçons du monde sur la manière moderne et nouvelles de faire fonctionner

135
00:06:04,580 --> 00:06:06,290
la politique

136
00:06:06,290 --> 00:06:11,799
ne sentent qu'il est en train de se livrer au plus malsain et au plus vils des copinages

137
00:06:12,590 --> 00:06:17,500
c'est par copinage que tout ce monde applaudit et d'accord et son enthousiasme à l'audition

138
00:06:17,720 --> 00:06:20,470
de celui qui se présente à cet instant comme un chef de bande

139
00:06:21,020 --> 00:06:23,020
davantage que comme un président de la république

140
00:06:23,510 --> 00:06:25,510
au demeurant que dit-il

141
00:06:25,970 --> 00:06:31,960
ce que les auditions démontré de runner il dit le seul responsable de cette affaire

142
00:06:32,570 --> 00:06:35,800
c'est moi et moi seul dit-il avant de

143
00:06:36,440 --> 00:06:38,440
déclarer hâbleur et font femmes

144
00:06:39,410 --> 00:06:42,309
s'ils veulent un responsable c'est moi et qu'il vienne me chercher

145
00:06:43,850 --> 00:06:46,029
Venir vous chercher monsieur le président de la république

146
00:06:46,790 --> 00:06:50,020
oui pourquoi pas dites nous où on doit aller vous chercher

147
00:06:51,110 --> 00:06:52,700
ceci est une

148
00:06:52,700 --> 00:06:54,700
auto montade qui n'a

149
00:06:55,070 --> 00:06:57,070
aucune réalité

150
00:06:57,650 --> 00:07:01,299
Soit on vous demande à vous auditionné et vous l'acceptez

151
00:07:02,300 --> 00:07:05,650
soit on ne peut rien parce que telle est la cinquième république alors pourquoi

152
00:07:06,080 --> 00:07:09,219
se donnait ses grands airs et dire je suis responsable de tout

153
00:07:09,740 --> 00:07:15,010
et je réponds de tous alors que vous ne répondez de rien vous ne répondez même pas d'effet

154
00:07:15,650 --> 00:07:17,860
car clairement vous avez tous menti

155
00:07:18,380 --> 00:07:23,679
vous monsieur le président de la république en disant qu'une sanction avait été prise mais il n'y en a aucune trace

156
00:07:24,620 --> 00:07:26,420
ni écrite

157
00:07:26,420 --> 00:07:32,409
ni dans le salaire de monsieur benalla il ya donc eu aucune sanction ne prononcez réellement

158
00:07:33,710 --> 00:07:40,839
votre ministre de l'intérieur mans quand il dit qu'il ne connaît pas monsieur benalla c'est impossible il a vu des dizaines de fois

159
00:07:41,840 --> 00:07:49,210
et cherchons à tour de rôle s'est déchargée sur le suivant et vous le premier parce que quand vous dites je me décharge pas

160
00:07:49,850 --> 00:07:52,960
sur des collaborateurs c'est pourtant ce que vous êtes en train de faire

161
00:07:53,570 --> 00:07:58,179
vous vous décharger sur les autres parce que les autres on peut les auditionner les autres peuvent

162
00:07:59,450 --> 00:08:05,559
être traduits en justice pas vous vous le savez vous vous décharger sur eux par conséquent

163
00:08:06,350 --> 00:08:12,850
la le comportement qu'a eu le président de la république hier soir est tout à fait inacceptable

164
00:08:14,760 --> 00:08:17,760
Nous sommes dans un régime de démocratie parlementaire

165
00:08:18,820 --> 00:08:22,499
et de présidentialisme très présidentialiste

166
00:08:23,320 --> 00:08:25,320
il faut respecter les formes

167
00:08:25,420 --> 00:08:27,420
tout ça n'est pas un jeu vidéo

168
00:08:27,790 --> 00:08:31,980
tout ça n'est pas un feuilleton dans lequel les personnages

169
00:08:33,190 --> 00:08:35,190
entre par une porte

170
00:08:35,200 --> 00:08:40,890
sorte par l'autre comme au théâtre de boulevard et pourtant c'est ce qu'ils sont en train de faire tous

171
00:08:41,680 --> 00:08:43,680
à 7h j'estime que

172
00:08:43,840 --> 00:08:48,509
le président devrait à l'honneur et au respect de l'institution parlementaire

173
00:08:49,420 --> 00:08:52,800
de se présenter pour une audition devant la commission

174
00:08:53,620 --> 00:09:00,000
des lois de l'assemblée nationale puisque nous sommes nombreux à le demander pour qu'il assume les décisions qu'il a prises

175
00:09:01,360 --> 00:09:08,820
pour ma part en écoutant ce qu'ils sont devenus je suis dorénavant convaincu d'au moins une chose

176
00:09:10,120 --> 00:09:12,450
le président par son comportement

177
00:09:13,420 --> 00:09:15,010
le président

178
00:09:15,010 --> 00:09:22,319
par la proposition de réforme constitutionnelle qu'ils présentent à décider définitivement de contourner le parlement

179
00:09:23,080 --> 00:09:26,699
de le considérer comme une réunion de famille

180
00:09:27,190 --> 00:09:34,919
dont le nombre doit être diminué dans les pouvoirs doivent être restreint dont les convocations doivent être totalement à la main

181
00:09:35,230 --> 00:09:38,130
du gouvernement car c'est le contenu de sa réforme

182
00:09:38,500 --> 00:09:46,169
constitutionnelle je considère comme un succès provisoire que cette réforme était été retiré je suis convaincu qu'à l'elysée

183
00:09:46,780 --> 00:09:48,780
ils organisaient

184
00:09:49,330 --> 00:09:57,239
sans aucun doute sur la question un nouveau service de sécurité du président de la république y auraient fonctionné comme une entité à part

185
00:09:57,670 --> 00:10:05,310
avec un corps particulier de fonctionnaires et de contractuels puisque monsieur benalla était un contractuel et que dans cette organisation

186
00:10:06,310 --> 00:10:11,250
alexandre benalla et quelques autres personnages qui ne sont pas des fonctionnaires de police ni des gendarmes

187
00:10:11,890 --> 00:10:14,640
joué un rôle essentiel tant et si bien

188
00:10:15,040 --> 00:10:20,790
qu'ils ont fini par se comporter comme si ce service existait déjà comme s'il en était déjà les chefs

189
00:10:21,139 --> 00:10:24,649
sans se rendre compte que tout ça n'avait aucune espèce de réalité

190
00:10:25,170 --> 00:10:27,170
administrative ni aucune légitimité

191
00:10:27,839 --> 00:10:29,839
politique

192
00:10:30,089 --> 00:10:36,349
Voilà le point où nous en sommes à bien sûr l'été peut passer là dessus le tour de france reprendra une de l'actualité

193
00:10:37,139 --> 00:10:39,139
les faits divers

194
00:10:39,239 --> 00:10:41,239
revenir devant mais la blessure

195
00:10:41,669 --> 00:10:43,669
dont est dorénavant

196
00:10:43,949 --> 00:10:45,119
affligé

197
00:10:45,119 --> 00:10:48,499
notre république elle ne cicatrisera pas si vite

198
00:10:49,529 --> 00:10:51,419
dans la vie des peuples

199
00:10:51,419 --> 00:10:57,559
il y a des moments qui sont fondateurs et des mouvements qui sont défaisant

200
00:10:58,230 --> 00:11:03,919
les moments fondateurs c'est quand le peuple lui même devient constituant en prend une décision à une très large majorité

201
00:11:04,799 --> 00:11:07,819
et les moments qui le des fonds ce sont des moments comme ceux-ci

202
00:11:08,100 --> 00:11:13,309
où tous les pouvoirs sont piétinés ou un ou deux personnages se croient au dessus de toutes les lois

203
00:11:13,679 --> 00:11:17,358
et ne respecte plus rien des règles qui font notre vie commune

204
00:11:17,540 --> 00:11:21,829
y avez vous vu comment tous ces gens parle dorénavant de l'opposition il la trouve insupportable

205
00:11:22,319 --> 00:11:26,808
mais dans une démocratie il ya une opposition toujours sinon c'est plus une démocratie

206
00:11:27,329 --> 00:11:29,160
c'est une dictature

207
00:11:29,160 --> 00:11:35,600
l'assemblée nationale n'est pas proposée à être qu'une fabrique des lois comme ils disent l'assemblée nationale vote la loi

208
00:11:36,239 --> 00:11:40,789
en principe est censé pouvoir l'amender ce qui ne se produit jamais avec la république en marche

209
00:11:41,220 --> 00:11:47,779
contrôler l'action du gouvernement et à une respiration politique elle s'exprime devant le peuple et lui montre elle donne à voir

210
00:11:48,209 --> 00:11:52,878
c'est cela que nous faisons c'est comme ça que ça se passe dans les sociétés civilisées alors on me dit

211
00:11:53,519 --> 00:11:57,019
c'est violent des fois oui c'est violent verbalement mais ça fait aussi partie

212
00:11:57,899 --> 00:11:59,899
de la façon de fonctionner tout ça

213
00:12:00,029 --> 00:12:04,998
autrefois on régler ses problèmes à coups de fusil ou un coup de bâton dans les règles avec des paroles avec des procédures

214
00:12:06,239 --> 00:12:08,009
et c'est pourquoi

215
00:12:08,009 --> 00:12:11,178
dans toute la vie publique le principe suprême est la vertu

216
00:12:12,239 --> 00:12:15,859
c'est à dire d'agir toujours conformément à ce qui est bon pour tous

217
00:12:16,649 --> 00:12:22,038
on ne peut pas séparer la vertu du droit dans une société d'état de droit

218
00:12:22,829 --> 00:12:25,399
le droit s'il n'est pas référer à l'intérêt général

219
00:12:26,130 --> 00:12:32,309
et à la vertu c'est plus le droit c'est juste des contrats et le contrat comme vous le savez c'est toujours le plus fort

220
00:12:32,500 --> 00:12:37,380
qui en décident les termes et c'est toujours le plus fort qui peut en vont pour les termes

221
00:12:39,190 --> 00:12:41,190
La france et en chinois

222
00:12:41,940 --> 00:12:44,489
ça s'appelle le pays de la loi c'est comme ça

223
00:12:45,630 --> 00:12:47,140
la loi

224
00:12:47,140 --> 00:12:48,490
est votée

225
00:12:48,490 --> 00:12:52,799
par tous par l'intermédiaire des députés qui représentent le peuple tout entier

226
00:12:54,250 --> 00:12:56,280
elle est placée sous l'égide de la vertu

227
00:12:57,250 --> 00:12:59,250
et elle s'applique à tous

228
00:13:00,010 --> 00:13:07,470
il pu arriver d'une manière qui se discute que certains personnages famille en quelque sorte hors du champ de la loi

229
00:13:08,710 --> 00:13:10,710
c'est le cadre du président de la république

230
00:13:11,650 --> 00:13:15,539
pour toutes sortes de raisons des fois qu'on peut comprendre d'autres moins

231
00:13:16,840 --> 00:13:18,220
mais là

232
00:13:18,220 --> 00:13:24,329
en plus de la loire il ya le sens commun il n'est pas normal que un individu se soustraient à toute question

233
00:13:25,330 --> 00:13:29,879
décide de faire des discours de bravache dans les pots de fin d'année pour narguer

234
00:13:30,790 --> 00:13:32,790
tout ce qui lui déplaisent

235
00:13:33,160 --> 00:13:35,160
montre du doigt d'une manière inacceptable

236
00:13:36,310 --> 00:13:41,280
l'opposition en disant qu'elle est l'ennemi de l'état et de la république pour l'instant

237
00:13:41,680 --> 00:13:48,330
dans ce contexte les amis de l'état c'est nous la france insoumise qu'il cessons de protester contre le démembrement de l'état

238
00:13:48,490 --> 00:13:50,490
au profit des sociétés privées

239
00:13:50,910 --> 00:13:58,469
les amis de la république ceni la france insoumise et d'autres bien sûr qui la faisons vivre à travers nos interpellations et notre action

240
00:13:59,530 --> 00:14:04,049
voilà je vous ai dit ces choses parce que au moment même où je vous parle

241
00:14:04,930 --> 00:14:06,880
j'apprends que

242
00:14:06,880 --> 00:14:08,999
imitant le style de leur grand chef

243
00:14:09,670 --> 00:14:17,520
dorénavant tous les chefs d'intermédiaire et chefaillons de la république en marche comporte comme lui tout à l'heure nous avons vu

244
00:14:17,830 --> 00:14:21,809
la présidente de la commission des lois et les autres membres de cette commission

245
00:14:22,090 --> 00:14:26,639
qui sont aussi membres de la république en marge tenir une conférence de presse séparée

246
00:14:27,100 --> 00:14:31,830
dans laquelle cette présidente annonce qu'en gros la commission

247
00:14:32,310 --> 00:14:36,570
dans kate a terminé son travail qu'elle ne recevra pas celui ci qu'elle n'entendra pas celui-là

248
00:14:36,880 --> 00:14:43,469
mais qu'elle va ré entendre celui ci est celui ci tout ça est inacceptable nous sommes pas un décor ils ne sont pas tout seul

249
00:14:43,779 --> 00:14:47,819
c'est pareil de décider seuls de ce qui se fera et ne se fera pas

250
00:14:48,910 --> 00:14:52,860
on voit bien à quoi ils veulent tous arrivés étant dans la sphère

251
00:14:53,950 --> 00:14:55,950
le président a parlé

252
00:14:56,050 --> 00:14:58,320
dans son pot de fin d'année avec ses copains

253
00:14:58,720 --> 00:15:04,020
vous avez vu derrière lui ranger tous les personnages de l'état le premier ministre et président de l'assemblée

254
00:15:04,089 --> 00:15:11,639
nationale la garde des sceaux et c'est le président du groupe et cette bande a décidé que l'affairé était close et elle est en train

255
00:15:11,980 --> 00:15:14,039
de la ferme et partout où elle le peut

256
00:15:14,650 --> 00:15:18,089
ce ne sera pas le cas ici à l'assemblée nationale car nous ne laisserons pas faire

257
00:15:18,930 --> 00:15:21,359
je ne sais pas quel sera le succès de nos interventions

258
00:15:21,940 --> 00:15:25,679
peut-être que vous entendrez dire que nous profitons des circonstances

259
00:15:26,290 --> 00:15:28,440
mais si nous ne le faisions pas s'il

260
00:15:28,570 --> 00:15:32,609
n'intervenait non pas comme nous le faisons mais c'est vous qui seriez en droit de dire mais qu'est ce que vous faites

261
00:15:32,790 --> 00:15:36,690
on vous a pas élus pour dire oui on vous a pas élus pour approuver

262
00:15:37,030 --> 00:15:41,309
tout ce qui se présente et vous taire quand on vous dit vous taire nous nous tairons pas

263
00:15:42,040 --> 00:15:44,040
nous finirons par avoir

264
00:15:44,230 --> 00:15:52,230
le dernier mot c'est à dire que cette réforme constitutionnelle ou bien ne se fera pas ou bien sera présenté au peuple français tout entier

265
00:15:52,660 --> 00:15:56,730
nous auront le dernier mot parce que la commission denquête si ce n'est celle de l'assemblée

266
00:15:57,040 --> 00:16:04,589
parce que la république en marche a réussi à la juguler à la bâillonner ce sera celle du sénat qui fera connaître la vérité

267
00:16:04,839 --> 00:16:10,529
tous ceux qui croient qu'ils peuvent contourner le parlement et le peuple se sont toujours trompés dans notre histoire et vous allez en avoir

268
00:16:10,720 --> 00:16:12,720
une nouvelle démonstration

269
00:16:12,820 --> 00:16:16,770
ayant accord notre république et vous verrez elle sera bien défendu

