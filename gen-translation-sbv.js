/* eslint-env es6 */
/* eslint semi: 2, no-unused-vars:1, no-console:1*/
// Standard lib requirements
var fs = require('fs'),
    readline = require('readline');

// Third party dependencies
var sax = require("sax");

// Local requirements
var my_util = require('./my-util.js');
// ({zeroFill, toSbvTime}) = require('./my-util.js');
var trace = console.log;
var warn = console.error;

var options = {};
var strict = true; // set to false for html-mode

var basename = "_9ijFgJnTh1M";
var sep_filename =  basename + ".sep";
// var sbv_filename = '/tmp/' + sep_filename.replace(/\.sep$/,'.sbv');
var sbv_filename = basename + ".en.sbv";
var tr_filename = basename + ".en.lines";

/*
 * Reading 2 streams in parallel may be overkill complication,
 * I will first load the translations in an array.
 */
var translations = [];
var index = 0;
readline.createInterface({
  input: fs.createReadStream(tr_filename)
    .on('error', (e) => {
      warn("error reading translations!", e);
    })
}).on('error', (e) => {
  warn("error parsing translations!", e);
}).on('line', (line) => {
  process.stdout.write(".");           // Trace progress
  translations.push(line);
  index += 1;
}).on('close', () => {
  trace("%s translations read", index);

  var sbv_stream = fs.createWriteStream(sbv_filename);

  var sep2sbv = sax.createStream(strict, options);
  sep2sbv.on("error", function (e) {
    // unhandled errors will throw, since this is a proper node
    // event emitter.
    warn("error!", e);
    // clear the error
    this._parser.error = null;
    this._parser.resume();
  });
  sep2sbv.on("opentag", function (node) {
    // opened a tag.  node has "name" and "attributes"
    // console.log('Open Tag %s', node.name);
    if ((node.name == 'subtitle') && (node.attributes.start)) {
      var tr = translations.shift();
      if (! tr) {
        throw(new Error('translations drained out', tr_filename, index));
      }
      trace("Subtitle #%s: %j", node.attributes.path, node.attributes.text);
      sbv_stream.write(`${my_util.toSbvTime(node.attributes.start)},${my_util.toSbvTime(node.attributes.end)}\n`);
      sbv_stream.write(tr + "\n\n");
      trace("  set to '%s'", tr);
    }
  });
  fs.createReadStream(sep_filename)
    .pipe(sep2sbv);
});
