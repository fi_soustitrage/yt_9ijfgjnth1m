/* eslint-env es6 */
/* eslint semi: 2, no-unused-vars:1, no-console:1*/
// Standard lib requirements
var fs = require('fs'),
    process = require('process'),
    readline = require('readline');



var paragraph = "";
var index = 0;
readline.createInterface({
  input: process.stdin
}).on('error', (e) => {
  console.error("error reading stdin", e);
}).on('line', (line) => {
  if (line && ! line.startsWith('0:')) {
    paragraph += line;
    if (line.trim().match('[.!?]$')) {
      index += 1;
      process.stdout.write(paragraph + "\n\n");
      paragraph = "";
    } else {
      paragraph += " ";
    }
  }
}).on('close', () => {
  process.stdout.write(paragraph);
});
