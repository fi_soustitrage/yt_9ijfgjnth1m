<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <!-- xmlns="http://www.w3.org/1999/xhtml" -->
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>

  <xsl:param name="video_id">9ijFgJnTh1M</xsl:param>
  <xsl:param name="title">Translation of Video <xsl:value-of select="$video_id"/></xsl:param>

  <xsl:template match="/SubtitleEditorProject">
    <html>
      <head>
        <title><xsl:value-of select="$title"/></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
        <style>
          .abstract {
          background-color: azure;
          padding: 20px;
          font-style: italic;
          color: #333;
          }
          #transcript {
          background-color: #eee;
          padding: 20px;
          }
        </style>

      </head>
      <body>
        <div class="container">
          <div class="row">
            <div class="col">
              <h1><xsl:value-of select="$title"/></h1>
              <div class="container abstract">
                <p> This is actually a list of subtitles, one line per subtitle block. </p>
                <p> The idea is to facilitate the reading of the phrasing. </p>
                <p> Temporal indications are intentionally omitted
                so as not to hinder concentration on the text. </p>
              </div>

              <div id="transcript">
                <xsl:apply-templates select="subtitles"/>
              </div>
            </div>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="subtitles">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="subtitle">
    <span class="subtitle-entry" data-start="{@start}" data-end="{@end}">
      <xsl:value-of select="@translation"/>
    </span>
    <br/>
  </xsl:template>

</xsl:stylesheet>
