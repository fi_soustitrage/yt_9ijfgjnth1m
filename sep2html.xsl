<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    >

  <!-- xmlns="http://www.w3.org/1999/xhtml" -->
  <xsl:output method="html" indent="yes" encoding="UTF-8"/>

  <xsl:param name="video_id">9ijFgJnTh1M</xsl:param>
  <xsl:param name="title">Transcription de la Video <xsl:value-of select="$video_id"/></xsl:param>

  <xsl:template match="/SubtitleEditorProject">
    <html>
      <head>
        <title><xsl:value-of select="$title"/></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
        <style>
          .abstract {
          background-color: azure;
          padding: 20px;
          font-style: italic;
          color: #333;
          }
          #transcript {
          background-color: #eee;
          padding: 20px;
          }
        </style>

      </head>
      <body>
        <div class="container">
          <div class="row">
            <div class="col">
              <h1><xsl:value-of select="$title"/></h1>
              <div class="container abstract">
                <p>Il s'agit en fait d'une liste de sous-titres, une ligne par bloc de sous-titre.</p>
                <p>L'idée est de faciliter la lecture du phrasé, mais aussi les traductions automatiques.</p>
                <p>Les indications temporelles sont intentionnellement omises
                pour ne pas gêner la concentration sur le texte.</p>
              </div>

              <div id="transcript">
                <xsl:apply-templates select="subtitles"/>
              </div>
            </div>
          </div>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="subtitles">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="subtitle">
    <span class="subtitle-entry" data-start="{@start}" data-end="{@end}">
      <xsl:value-of select="@text"/>
    </span>
    <br/>
  </xsl:template>

</xsl:stylesheet>
