/* eslint-env es6 */
/* eslint semi: 1, no-unused-vars:0, no-console:0*/
// Standard lib requirements
var fs = require('fs'),
    readline = require('readline');

// Third party dependencies
var sax = require("sax");

// Local requirements
var my_util = require('./my-util.js');
// ({zeroFill, toSbvTime}) = require('./my-util.js');

var options = {};
var strict = true; // set to false for html-mode

var sep_filename = "_9ijFgJnTh1M.sep";
var sbv_filename = '/tmp/' + sep_filename.replace(/\.sep$/,'.sbv');

var sbv_stream = fs.createWriteStream(sbv_filename);

var sep2sbv = sax.createStream(strict, options);
sep2sbv.on("error", function (e) {
  // unhandled errors will throw, since this is a proper node
  // event emitter.
  console.error("error!", e);
  // clear the error
  this._parser.error = null;
  this._parser.resume();
});
sep2sbv.on("opentag", function (node) {
  // opened a tag.  node has "name" and "attributes"
  // console.log('Open Tag %s', node.name);
  if ((node.name == 'subtitle') && (node.attributes.start)) {
    sbv_stream.write(`${my_util.toSbvTime(node.attributes.start)},${my_util.toSbvTime(node.attributes.end)}\n`);
    sbv_stream.write(node.attributes.text + "\n\n");
  }
});

// pipe is supported, and it's readable/writable
// same chunks coming in also go out.
fs.createReadStream(sep_filename)
  .pipe(sep2sbv);
// .pipe(fs.createWriteStream("file-copy.xml"));
